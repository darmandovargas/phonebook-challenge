/* Phonebook */
var app = angular.module('app', ['angularUtils.directives.dirPagination']);
app.controller('ctrl', function($scope, $http) {
    
		
	
	$scope.form = {
			firstName: "",
		    lastName: "",
		    phone:""
	};
	
	var originalForm = angular.copy($scope.form);
	
	$scope.resetForm = function (){
		console.log("Reset Form...");
		$scope.form = angular.copy(originalForm);
		$scope.contactForm.$setPristine();
    };
    
    $scope.sort = function(keyname){
        $scope.sortKey = keyname;   //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }
    
    $scope.findAll = function(){
    	$http.get('/api/findAll').then(
    			function successCallback(response){				
    				$scope.contacts = response.data;				
    			}, function errorCallback(response){
    				console.log(response); 
    	});	
    }
    
    $scope.findAll();
    
    $scope.saveForm = function(){    	
    	var data ={
            firstName: $scope.form.firstName,
            lastName: $scope.form.lastName,
            phone: $scope.form.phone 
        };
        
        console.log(data);
    	
    	var config = {
                headers : {
                    'Content-Type': 'application/json'
                }
            }
    	
    	$http.post('/api/save', data, config).then(
    			function successCallback(response){
    				$scope.findAll();	
    				$scope.resetForm();
    				$scope.message = "Contact saved successfuly";
    			}, function errorCallback(response){
    				$scope.message = "Error saving the contact"; 
    	});	
    }
    
    $scope.searchForm = function(){    	
    	var data ={
            firstName: $scope.form.firstName,
            lastName: $scope.form.lastName,
            phone: $scope.form.phone 
        };
        
        console.log(data);
    	
    	var config = {
                headers : {
                    'Content-Type': 'application/json'
                }
            }
    	
    	var patternSearch = "-";
    	
    	if($scope.form2.pattern != ""){
    		patternSearch = $scope.form2.pattern;
    	}
    	 
    	$http.get('/api/search/'+patternSearch).then(
    			function successCallback(response){
    				console.log(response.data);
    				$scope.contacts = response.data;    				
    			}, function errorCallback(response){
    				$scope.messageSearch = "Error searching the contact"; 
    	});	
    }
});
