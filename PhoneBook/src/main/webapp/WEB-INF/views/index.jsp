<%@ page language="java" contentType="text/html; charset=US-ASCII" pageEncoding="US-ASCII"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title>PhoneBook</title>
<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.6.0/pure-min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="css/styles.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css">

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
<script src="/js/dirPaginator.js"></script>
<script src="/js/phonebook.js"></script>
</head>
<body>
	<div class="container" ng-app="app" ng-controller="ctrl">
		<div class="pure-g">
			<div class="pure-u-1">
				<div class="header">
					<img class="logo" src="img/phonebook.png" />
					<p>v 1.0</p>
				</div>

			</div>
		</div>
		<div class="pure-g">
			<div class="pure-u-sm-1 pure-u-1-3">
				<div class="box">
					<h2>
						<i class="fa fa-user-plus"></i>New contact
					</h2>
					<form name="contactForm" class="pure-form" ng-submit="saveForm()">
						<fieldset class="pure-group">
							<input type="text" class="pure-input-1-2" placeholder="First Name" ng-model="form.firstName" ng-required="true">
							<input type="text" class="pure-input-1-2" placeholder="Last Name" ng-model="form.lastName" ng-required="true">
							<input type="text" class="pure-input-1-2" placeholder="Phone" ng-model="form.phone" ng-required="true">
						</fieldset>
						<button ng-disabled="contactForm.$invalid" type="submit" class="pure-button pure-input-1-2 pure-button-primary">
							<i class="fa fa-user-plus"></i>Add
						</button>
						<br></br>
						<div class="sample-show-hide">{{ message }}</div>
					</form>
				</div>
			</div>

			<div class="pure-u-sm-1 pure-u-1-3">
				<div class="box">
					<h2>
						<i class="fa fa-search"></i>Search contact
					</h2>
					<form class="pure-form" ng-submit="searchForm()">
						<fieldset class="pure-group">
							<input type="text" class="pure-input-1-2" ng-model="form2.pattern">
						</fieldset>
						<button type="submit" class="pure-button pure-input-1-2 pure-button-primary">
							<i class="fa fa-search"></i>Search
						</button>
						<br></br>
						<div class="sample-show-hide">{{ messageSearch }}</div>
					</form>
				</div>
			</div>
			<div class="pure-u-sm-1 pure-u-1-3">
				<div class="box" align="center">
					<h2>
						<i class="fa fa-users"></i> Contacts
					</h2>
					<table class="pure-table" width="100%">
						<thead>
							<tr>
								<th width="33.33%" align="center">First Name</th>
								<th width="33.33%" align="center">Last Name</th>
								<th width="33.33%" align="center">Phone</th>
							</tr>
						</thead>
						<tbody>
							<tr dir-paginate="c in contacts|orderBy:sortKey:reverse|filter:search|itemsPerPage:4">
								<td>{{ c.firstName }}</td>
								<td>{{ c.lastName }}</td>
								<td>{{ c.phone }}</td>
							</tr>
						</tbody>
					</table>
					<dir-pagination-controls max-size="4" direction-links="true" boundary-links="true"> </dir-pagination-controls>
				</div>
			</div>
		</div>
	</div>
</body>
</html>