package com.livevox.phonebook.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.livevox.phonebook.model.Contact;
import com.livevox.phonebook.service.IContactService;
import java.util.List;

@RestController
@RequestMapping({"/api"})
public class ContactCtrl {

    @Autowired
    private IContactService iContactService;
    
    @PostMapping(path="/save", consumes = "application/json")
    public Contact save(@RequestBody Contact contact){
        return iContactService.saveSrv(contact);
    }
    
    @GetMapping(path = {"/search/{pattern}"})
    public List<Contact> search(@PathVariable("pattern") String pattern){
    	if(pattern.equals("-")) {
    		 return iContactService.findAllSrv();
    	}else {
    		return iContactService.searchSrv(pattern);
    	}        
    }
    
    @GetMapping(path = {"/findAll"})
    public List<Contact> findAll(){
        return iContactService.findAllSrv();
    }
    
    @DeleteMapping(path ={"/{id}"})
    public Contact delete(@PathVariable("id") int id) {
        return iContactService.deleteSrv(id);
    }

    @GetMapping(path = {"/{id}"})
    public Contact findOne(@PathVariable("id") int id){
        return iContactService.findByIdSrv(id);
    }

    @PutMapping
    public Contact update(@RequestBody Contact contact){
        return iContactService.updateSrv(contact);
    }   
}
