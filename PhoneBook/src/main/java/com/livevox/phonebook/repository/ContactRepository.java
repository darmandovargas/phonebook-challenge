package com.livevox.phonebook.repository;

import org.springframework.data.repository.Repository;

import com.livevox.phonebook.model.Contact;

import java.util.List;

public interface ContactRepository extends Repository<Contact, Integer> {

	Contact save(Contact contact);
	
	List<Contact> findByFirstNameLikeOrLastNameLikeOrPhoneLike(String firstName, String lastName, String Phone);
	
	List<Contact> findAll();
	
	void delete(Contact contact);    

    Contact findOne(int id);
}