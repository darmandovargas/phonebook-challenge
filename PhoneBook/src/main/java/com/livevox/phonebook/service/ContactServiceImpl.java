package com.livevox.phonebook.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.livevox.phonebook.model.Contact;
import com.livevox.phonebook.repository.ContactRepository;

import java.util.List;

@Service
public class ContactServiceImpl implements IContactService {

    @Autowired
    private ContactRepository repository;

    @Override
    public Contact saveSrv(Contact contact) {
        return repository.save(contact);
    }
    
    @Override
    public List<Contact> searchSrv(String pattern) {
        return repository.findByFirstNameLikeOrLastNameLikeOrPhoneLike('%'+pattern+'%', '%'+pattern+'%', '%'+pattern+'%');
    }
    
    @Override
    public List<Contact> findAllSrv() {
        return repository.findAll();
    }

    @Override
    public Contact deleteSrv(int id) {
        Contact contact = findByIdSrv(id);
        if(contact != null){
            repository.delete(contact);
        }
        return contact;
    }   

    @Override
    public Contact findByIdSrv(int id) {
        return repository.findOne(id);
    }

    @Override
    public Contact updateSrv(Contact contact) {
        return null;
    }
}
