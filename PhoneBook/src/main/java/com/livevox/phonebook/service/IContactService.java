package com.livevox.phonebook.service;

import java.util.List;

import com.livevox.phonebook.model.Contact;

public interface IContactService {

    Contact saveSrv(Contact contact);
    
    List<Contact> searchSrv(String pattern);
    
    List<Contact> findAllSrv();

    Contact deleteSrv(int id);    

    Contact findByIdSrv(int id);

    Contact updateSrv(Contact contact);
}
